import supertest from "supertest";

import app from "./server";
const request = supertest(app);

it("Call the /home endpoint", async (done) => {
  const res = await request.get("/home");
  expect(res.status).toBe(200);
  expect(res.text).toBe("Ini Homepage!");
  done();
});
it("Call the / endpoint", async (done) => {
  const res = await request.get("/");
  expect(res.status).toBe(200);
  expect(res.text).toBe("This App is running properly!");
  done();
});
it("Call the /ping endpoint", async (done) => {
  const res = await request.get("/ping");
  expect(res.status).toBe(200);
  expect(res.text).toBe("sucess 200!");
  done();
});
it("Call the /hello/:name endpoint", async (done) => {
  const res = await request.get("/hello/user");
  expect(res.status).toBe(200);
  expect(res.body.message).toBe("Hello user");
  done();
});
it("Call the /yunan endpoint", async (done) => {
  const res = await request.get("/yunan");
  expect(res.status).toBe(200);
  expect(res.text).toBe("project cicd by yunan!");
  done();
});
